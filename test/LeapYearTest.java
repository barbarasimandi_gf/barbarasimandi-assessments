import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LeapYearTest {

    LeapYear yearInQuestion;
    int leapYear, notLeapYear, zero, bc;

    @Before
    public void setUp() throws Exception {
        leapYear = 1992;
        notLeapYear = 1990;
        zero = 0;
        bc = -12;
    }

    @Test
    public void testLeapYear() throws Exception {
        yearInQuestion = new LeapYear(leapYear);
        assertTrue(yearInQuestion.isLeapYear());
    }

    @Test
    public void testNotLeapYear() throws Exception {
        yearInQuestion = new LeapYear(notLeapYear);
        assertFalse(yearInQuestion.isLeapYear());
    }

    @Test
    public void testZero() throws Exception {
        yearInQuestion = new LeapYear(zero);
        assertTrue(yearInQuestion.isLeapYear());
    }

    @Test
    public void testBC() throws Exception {
        yearInQuestion = new LeapYear(bc);
        assertTrue(yearInQuestion.isLeapYear());
    }

}