import org.junit.Before;
import org.junit.Test;
import java.time.LocalDate;
import java.time.Month;
import static org.junit.Assert.*;

public class GigaSecondTest {

    GigaSecond birthDate;
    String theDateInRaw;
    String theDate;

    @Before
    public void setUp() throws Exception {
        birthDate = new GigaSecond(LocalDate.of(1990, Month.MAY, 3));
        theDateInRaw = "2022-01-09T01:46:40";
        theDate = "2022.01.09";
    }

    @Test
    public void testCountDate() throws Exception {
        assertEquals(theDateInRaw, birthDate.countDate().toString());
    }

    @Test
    public void testOutputDate() throws Exception {
        assertEquals(theDate, birthDate.toString());
    }
}