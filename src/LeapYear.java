public class LeapYear {

    int yearInQuestion;

    public LeapYear(int yearInQuestion) {
        this.yearInQuestion = yearInQuestion;
    }

    public boolean isLeapYear() {
        return ((yearInQuestion % 4 == 0) && (yearInQuestion % 100 != 0) || (yearInQuestion % 400 == 0));
    }
}
