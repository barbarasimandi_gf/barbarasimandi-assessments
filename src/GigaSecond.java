import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class GigaSecond {

    LocalDateTime birthDate;

    public GigaSecond(LocalDate birthDate) {
        this.birthDate = birthDate.atTime(0, 0);
    }

    public LocalDateTime getBirthDate() {
        return birthDate;
    }

    public LocalDateTime countDate() {
        int oneGigaSecond = 1000000000;
        LocalDateTime outputDate = getBirthDate().plusSeconds(oneGigaSecond);
        return outputDate;
    }

    @Override
    public String toString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.DD");
        return countDate().format(formatter);
    }
}
