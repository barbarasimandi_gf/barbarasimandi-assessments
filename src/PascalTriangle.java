public class PascalTriangle {

    long row;

    public PascalTriangle(int row) {
        this.row = row;
    }

    public long getRow() {
        return row;
    }

    public void draw() {
        long row = getRow();
        long number;

        for (long i = 0; i < row; i++) {
            for (long k = row; k > i; k--) {
                System.out.print(" ");
            }
            number = 1;
            for (long j = 0; j <= i; j++) {
                System.out.print(number + " ");
                number = number * (i - j) / (j + 1);
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        PascalTriangle triangle = new PascalTriangle(31);
        triangle.draw();
    }
}
